# Imageviewer, should ultimately be implanted in FWB

import Tkinter as tk
import ttk
import tkSimpleDialog
import tkFileDialog
import sqlite3 as sql
from PIL import Image, ImageTk
import glob

def doNothing():
    print "Not yet implemented!"

def populateImageList(path):
    '''
    Function to target a specific map and populate the ImageList
    '''
    filetypes = ["jpg"]
    imageList = []
    try:
        for extension in filetypes:
            imageList.extend(glob.glob(path + "*." + extension))
    except:
        imageList = []
    return imageList

def populateDB(dbName,nameList):
    '''
    Function to populate the db with id's and path
    '''
    conn = sql.connect(dbName)
    c = conn.cursor()

    for imagePath in nameList:
        c.execute("INSERT INTO Images (path,location_id,description) values (?,?,?);", (imagePath,"NG-002",""))

    conn.commit()

class ImViewer(tk.Tk):
    def __init__(self, *args, **kwargs):

        tk.Tk.__init__(self, *args, **kwargs)

        tk.Tk.wm_title(self, "IMage VIEWER")

        menubar = tk.Menu(self)
        tk.Tk.config(self, menu=menubar)

        fileMenu = tk.Menu(menubar)
        fileMenu.add_command(label="Exit",command=doNothing)
        menubar.add_cascade(label="File", menu=fileMenu)

        conn = sql.connect("testdb.db")
        self.c = conn.cursor()

        self.travelNumber = 0
        self.dbName = "testdb.db"

        self.mapImages = ""
        self.mapImages = self.mapImages + ""
        self.outcropList = ["NG-001","NG-002","NG-003"]

        populateDB("testdb.db",populateImageList(self.mapImages))

        f = self.c.execute("SELECT min(id) FROM Images")
        self.min_imageId = f.fetchone()[0]

        f = self.c.execute("SELECT max(id) FROM Images")
        self.max_imageId = f.fetchone()[0]

        self.imageId = self.min_imageId

        f = self.c.execute("SELECT * FROM Images WHERE id=?",[self.imageId])
        f = f.fetchone()

        self.v = tk.StringVar()
        self.v.set(f[2])
        self.outcropOptionList = tk.OptionMenu(self, self.v, *self.outcropList)

        self.label_id = tk.Label(self, text="Id")
        self.label_namePic = tk.Label(self, text="Picture")
        self.label_outcrop = tk.Label(self, text="Outcrop")
        self.label_description = tk.Label(self, text="Description")

        self.button_update = tk.Button(self, text="Update", command=self.updateImageRow)

        self.entry_id = tk.Entry(self)
        self.entry_id.insert(10,f[0])
        self.entry_id.configure(state="disabled")

        #entry_outcrop = tk.Entry(self)
        #entry_outcrop.insert(10,"NG-001")
        self.entry_description = tk.Text(self,height=10)
        self.entry_description.insert("1.0",f[3])

        self.label_id.grid(row=0, column=0, sticky="W", padx=5, pady=5)
        self.label_namePic.grid(row=1, column=0, sticky="W", padx=5, pady=5)
        self.label_outcrop.grid(row=2, column=0, sticky="W", padx=5, pady=5)
        self.label_description.grid(row=3, column=0, sticky="W", padx=5, pady=5)
        self.button_update.grid(row=5, column=0, columnspan=2, sticky=tk.S)

        self.entry_id.grid(row=0, column=1, sticky=tk.W, padx=5, pady=5)

        self.outcropOptionList.grid(row=2, column=1, sticky=tk.W, padx=2, pady=5)
        #entry_outcrop.grid(row=2, column=1, sticky=tk.W, padx=5, pady=5)
        self.entry_description.grid(row=4, column=0, columnspan=2, sticky="W", padx=5, pady=5)

        self.imageList = []


        self.imageList = populateImageList(self.mapImages)

        self.namePicShort = f[1].split("\\")[-1]
        self.entry_namePic = tk.Entry(self)

        self.entry_namePic.insert(20,self.namePicShort)
        self.entry_namePic.configure(state="disabled")
        self.entry_namePic.grid(row=1, column=1, sticky="W", padx=5, pady=5)
        image = Image.open(self.imageList[0])
        width_standard = 700.0
        aspect = width_standard/image.size[0]
        image_processed = image.resize((int(width_standard),int((image.size[1]) * aspect)),Image.ANTIALIAS)
        photo = ImageTk.PhotoImage(image_processed)





        self.label1 = tk.Label(image=photo)
        self.label1.image = photo # keep a reference!
        self.label1.grid(row=0, column=3, rowspan=6, sticky="N")


        self.button_prev = tk.Button(self, text=u"\u2190",command=lambda:self.travelImage(False))
        self.button_next = tk.Button(self, text=u"\u2192",command=lambda:self.travelImage(True))
        #slider = tk.Scale(self, from_=0, to=100, orient="horizontal")

        self.button_prev.grid(row=7, column=3, sticky=tk.W+tk.N)
        self.button_next.grid(row=7, column=3, sticky=tk.E+tk.N)
        #slider.grid(row=6, column=3, sticky="S")

        #self.bind('<Right>',lambda event, forward=True:self.travelImage(event,forward))
        #self.bind('<Left>',lambda event, forward=False:self.travelImage(event,forward))


    def travelImage(self, direction):
        '''
        This function is used to travel through the images in a directory.

        direction: True = next ; False = previous
        '''
        if direction:
            self.imageId += 1
            if self.imageId > self.max_imageId:
                self.imageId = self.min_imageId

        else:
            self.imageId -= 1
            if self.imageId < self.min_imageId:
                self.imageId = self.max_imageId

        f = self.c.execute("SELECT * FROM Images WHERE id=?",[self.imageId])
        f = f.fetchone()

        self.newImage = f[1]
        self.namePicShort = f[1].split("\\")[-1]
        image = Image.open(self.newImage)
        width_standard = 700.0
        aspect = width_standard/image.size[0]
        image_processed = image.resize((int(width_standard),int((image.size[1]) * aspect)),Image.ANTIALIAS)
        photo = ImageTk.PhotoImage(image_processed)
        self.label1.configure(image = photo)
        self.label1.image = photo

        self.entry_namePic.config(state="normal")
        self.entry_namePic.delete(0, "end")
        self.entry_namePic.insert(20,self.namePicShort)
        self.entry_namePic.config(state="disabled")

        self.entry_id.config(state="normal")
        self.entry_id.delete(0, "end")
        self.entry_id.insert(20,f[0])
        self.entry_id.config(state="disabled")

        self.v.set(f[2])
        if len(self.entry_description.get("1.0","end")) != 1:
            self.entry_description.delete("1.0","end")
        self.entry_description.insert("1.0", f[3])
        self.entry_description.mark_set("insert","1.0")

    def updateImageRow(self):
        '''
        Function to update the image row
        '''
        conn = sql.connect(self.dbName)
        c = conn.cursor()

        c.execute("UPDATE Images SET location_id=?, description=? WHERE id=?", (self.v.get(),self.entry_description.get("1.0","end"),self.entry_id.get()))
        conn.commit()



app = ImViewer()

app.rowconfigure(5, weight=1)
app.columnconfigure(4, weight=2)
app.columnconfigure(1, weight=1)
app.rowconfigure(7, weight=9999)
app.geometry("1024x600")
app.iconbitmap('fwb.ico')


app.mainloop()
