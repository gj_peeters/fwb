# GUI for FieldWork Buddy

import Tkinter as tk
import tkSimpleDialog
import tkFileDialog
import sqlite3 as sql


def doNothing():
    print "Not yet supported!"

def newDB():
	name = tkSimpleDialog.askstring("Name", "Name of the new project?")
	name = "%s.db" % name
	conn = sql.connect(name)
	c = conn.cursor()

	c.execute('''
	CREATE TABLE 'Location' (
	`id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
	`label`	TEXT UNIQUE,
	`locX`	REAL,
	`locY`	REAL,
	`dt`	TEXT,
	`description`	TEXT)

    CREATE TABLE 'Deposit' (
    'id' INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
    'location_id' INTEGER UNIQUE,
    'place' INTEGER,
    'thickness' REAL,
    'type_id' INTEGER,
    'description' TEXT,
    'stratigraphy_id' INTEGER,
    'eruption_id' INTEGER,
    )

    CREATE TABLE 'Stratigraphy' (
    'id' INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
    'label' TEXT UNIQUE,
    'type_id' INTEGER,
    'eruption_id' INTEGER,
    'description' TEXT,  
    )
	''')

def openDB():
	name = tkFileDialog.askopenfilename()
	print name

class FWB(tk.Tk):
    def __init__(self, *args, **kwargs):

        tk.Tk.__init__(self, *args, **kwargs)

        tk.Tk.wm_title(self, "FieldWork Buddy")

        container = tk.Frame(self)
        container.pack(side="top", fill="both", expand=True)
        container.grid_rowconfigure(0, weight=1)
        container.grid_columnconfigure(0, weight=1)

        menubar = tk.Menu(container)

        projectMenu = tk.Menu(menubar,tearoff=0)
        projectMenu.add_command(label="New...", command=newDB)
        projectMenu.add_command(label="Open...", command=openDB)
        projectMenu.add_command(label="Save...", command=doNothing)
        projectMenu.add_command(label="Save as...", command=doNothing)
        projectMenu.add_separator()
        projectMenu.add_command(label="Exit", command=quit)
        menubar.add_cascade(label="Project", menu=projectMenu)

        importMenu = tk.Menu(menubar,tearoff=0)
        importMenu.add_command(label="Locations...", command=doNothing)
        importMenu.add_command(label="Deposits...", command=doNothing)
        importMenu.add_command(label="Volcanoes...", command=doNothing)
        importMenu.add_command(label="Stratigraphy...", command=doNothing)
        importMenu.add_command(label="Type...", command=doNothing)
        menubar.add_cascade(label="Import", menu=importMenu)

        addMenu = tk.Menu(menubar,tearoff=0)
        addMenu.add_command(label="Locations...", command=doNothing)
        addMenu.add_command(label="Deposits...", command=doNothing)
        addMenu.add_command(label="Volcanoes...", command=doNothing)
        addMenu.add_command(label="Stratigraphy...", command=doNothing)
        addMenu.add_command(label="Type...", command=doNothing)
        menubar.add_cascade(label="Add", menu=addMenu)

        tk.Tk.config(self, menu=menubar)


    # -- TOOLBAR --
    #toolbar = tk.Frame(root)

    #insertButton = tk.Button(toolbar, text="Insert Image", command=doNothing)
    #insertButton.pack(side="left", padx=2)
    #printButton = tk.Button(toolbar, text="Print", command=doNothing)
    #printButton.pack(side="left", padx=2)

    #toolbar.pack(side="top", fill="x")

    # -- STATUSBAR --
    #status = tk.Label(root, text="Do Nothing", bd=1, relief="sunken", anchor="w")
    #status.pack(side="bottom", fill="x")



app = FWB()
app.geometry("1024x600")

app.mainloop()
